'use strict';

// Реализуем вектор

class Vector {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  // Создаем вектор координаты которого будут сложены

  plus(vector) {
    if (!(vector instanceof Vector)) {
      throw new Error('Можно прибавлять к вектору только вектор типа Vector');
    }
    return new Vector(this.x + vector.x, this.y + vector.y);
  }

  // Создаем вектор координаты которого будут умножены на множитель

  times(factor) {
    return new Vector(this.x * factor, this.y * factor);
  }
}

// Движущийся объект

class Actor {
  constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
    if (!(pos instanceof Vector)) {
      throw new Error('Свойство не определено');
    }

    if (!(size instanceof Vector)) {
      throw new Error('Свойство не определено');
    }

    if (!(speed instanceof Vector)) {
      throw new Error('Свойство не определено');
    }

    this.pos = pos;
    this.size = size;
    this.speed = speed;
  }

  get type() {
    return 'actor';
  }

  act() {}

  get left() {
    return this.pos.x;
  }

  get right() {
    return this.pos.x + this.size.x;
  }

  get top() {
    return this.pos.y;
  }

  get bottom() {
    return this.pos.y + this.size.y;
  }

  // Проверка, пересекается ли объект Actor с другим

  isIntersect(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error(`Не является типом Actor: ${actor}`);
    }

    if (this === actor) {
      return false;
    }

    const hosPos = actor.left === this.left && actor.right === this.right;
    const verPos = actor.top === this.top && actor.bottom === this.bottom;
    const posX = (actor.left < this.left && this.left < actor.right) || (this.left < actor.left && actor.left < this.right);
    const posY = (actor.top < this.top && this.top < actor.bottom) || (this.top < actor.top && actor.top < this.bottom);
    return (posX && posY) || (posX && verPos) || (posY && hosPos) || (hosPos && verPos);
  }

}

// Уровень

class Level {
  constructor(grid = [], actors = []) {
    for (const actor of actors) {
      if (actor.type === 'player') {
        this.player = actor;
        break;
      }
    }

    this.grid = grid;
    this.actors = actors;
    this.height = grid.length;
    this.width = 0;

    if (grid.length !== 0) {
      for (const arr of this.grid) {
        if (typeof arr != 'undefined') {
          if (this.width < arr.length) {
            this.width = arr.length;
          }
        }
      }
      this.grid.forEach((element) => {
        if (Array.isArray(element))
          this.width = element.length;
        this.width = element.length;
      })
    }

    this.status = null;
    this.finishDelay = 1;
  }

  // Проверка завершения уровня

  isFinished() {
    return (this.status !== null && this.finishDelay < 0);
  }

  // Проверка, находится ли на позиции другой объект

  actorAt(actor) {
    if (!(actor instanceof Actor)) {
      throw new Error('Не является типом Actor');
    }

    if (this.grid === undefined) {
      return;
    }

    for (const act of this.actors) {
      if (typeof act != 'undefined' && actor.isIntersect(act)) {
        return act;
      }
    }

    return;
  }

  // Не даем объекту покинуть игровое поле

  obstacleAt(pos, size) {
    if (!(pos instanceof Vector)) {
      throw 'Не является типом Actor';
    }

    if (!(size instanceof Vector)) {
      throw 'Не является типом Actor';
    }

    const xStart = Math.floor(pos.x);
    const xEnd = Math.ceil(pos.x + size.x),
      yStart = Math.floor(pos.y),
      yEnd = Math.ceil(pos.y + size.y);

    if (xStart < 0 || xEnd > this.width || yStart < 0) {
      return 'wall';
    }

    if (yEnd > this.height) {
      return 'lava';
    }

    for (let y = yStart; y < yEnd; y++) {
      for (let x = xStart; x < xEnd; x++) {
        const obstacle = this.grid[y][x];
        if (typeof obstacle !== 'undefined') {
          return obstacle;
        }
      }
    }
    return;
  }

  // Удалить движущийся объект

  removeActor(actor) {
    const indexActor = this.actors.indexOf(actor);
    if (indexActor !== -1) {
      this.actors.splice(indexActor, 1);
    }
  }

  // Проверка наличия объектов на игровом поле

  noMoreActors(type) {
    if (this.actors) {
      for (let actor of this.actors) {
        if (actor.type === type) {
          return false;
        }
      }
    }
    return true;
  }

  // Меняем игровой процесс.
  playerTouched(type, actor) {
    if (this.status !== null) {
      return;
    }

    if (type === 'lava' || type === 'fireball') {
      this.status = 'lost';
    }

    if (type === 'coin' && actor.type === 'coin') {
      this.removeActor(actor);
      if (this.noMoreActors('coin')) {
        this.status = 'won';
      }
    }
  }
}

class LevelParser {
  constructor(dictionary) {
    this.dictionary = dictionary;
  }

  // Получаем объект по символу

  actorFromSymbol(symbol) {
    if (typeof symbol === 'undefined') {
      return;
    }

    if (typeof this.dictionary === 'undefined') {
      return;
    }

    return this.dictionary[symbol];
  }

  // Получаем тип препятствия по символу

  obstacleFromSymbol(symbol) {
    const symbols = {
      'x': 'wall',
      '!': 'lava'
    };
    return symbols[symbol];
  }

  // Создаем сетку игрового поля без движущихся объектов

  createGrid(strings) {
    const array = [];
    let i = 0;

    for (const string of strings) {
      array[i] = [];
      for (let j = 0; j < string.length; j++) {
        const symbol = string.charAt(j);
        if (symbol === 'o' || symbol === 'x' || symbol === '!') {
          array[i].push(this.obstacleFromSymbol(symbol));
        } else {
          array[i].push(undefined);
        }
      }

      i++;
    }

    return array;
  }

  // Создаем сетку движущихся объектов игрового поля

  createActors(strings) {
    const array = [];
    let j = 0;

    for (let k = 0; k < strings.length; k++) {
      const string = strings[k];
      for (let i = 0; i < string.length; i++) {
        const symbol = string.charAt(i);
        const actorCtr = this.actorFromSymbol(symbol);
        if (typeof actorCtr === 'function') {
          const actor = new actorCtr();
          if (actor instanceof Actor) {
            array[j] = new actorCtr();
            array[j].pos = new Vector(i, k);
            j++;
          }
        }
      }
    }

    return array;
  }

  // Создаем игровое поля из сетки

  parse(strings) {
    return new Level(this.createGrid(strings), this.createActors(strings));
  }
}

// Реализация шаровой молнии

class Fireball extends Actor {
  constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
    super(pos, new Vector(1, 1), speed);
  }

  get type() {
    return 'fireball';
  }

  // Следующая позиция шаровой молнии

  getNextPosition(time = 1) {
    return this.pos.plus(this.speed.times(time));
  }

  // Если столкнулись с препятствием

  handleObstacle() {
    this.speed = this.speed.times(-1);
  }

  // Обновляем состояние

  act(time, level) {
    const nextPos = this.getNextPosition(time);
    if (level.obstacleAt(nextPos, this.size)) {
      this.handleObstacle();
    } else {
      this.pos = nextPos;
    }
  }
}

// Горизонтальная шаровая молния

class HorizontalFireball extends Fireball {
  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(2, 0));
  }
}

// Вертикальная шаровая молния

class VerticalFireball extends Fireball {
  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(0, 2));
  }
}

// Огненный дождь

class FireRain extends Fireball {
  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(0, 3));
    this.initPos = pos;
  }

  get type() {
    return 'firerain';
  }

  // Если столкнулись с препятствием

  handleObstacle() {
    this.pos = this.initPos;
  }
}

// Реализация монетки

class Coin extends Actor {
  constructor(pos = new Vector(1, 1)) {
    super(new Vector(pos.x + 0.2, pos.y + 0.1), new Vector(0.6, 0.6));
    this.initPos = pos;
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.random() * 2 * Math.PI
  }

  get type() {
    return 'coin';
  }

  // Обновляем фазу прыжка

  updateSpring(time = 1) {
    this.spring += this.springSpeed * time;
  }

  // Получаем вектор прыжка

  getSpringVector() {
    return new Vector(0, Math.sin(this.spring) * this.springDist);
  }

  // Получаем следующуюю позицию

  getNextPosition(time = 1) {
    this.updateSpring(time);
    const springVector = this.getSpringVector();
    return new Vector(this.pos.x, this.pos.y + springVector.y * time);
  }

  // Новая позиция монетки

  act(time) {
    this.pos = this.getNextPosition(time);
  }
}

// Объект игрока

class Player extends Actor {
  constructor(pos = new Vector(1, 1)) {
    super(new Vector(pos.x, pos.y - 0.5), new Vector(0.8, 1.5));
  }

  get type() {
    return 'player';
  }
}

// Сетки уровня

const schemas = [
  [
    '         ',
    '   h     ',
    '         ',
    '       o ',
    '@     xxx',
    '         ',
    'xxx      ',
    '         '
  ],
  [
    '   v     ',
    '         ',
    '         ',
    '@       o',
    '        x',
    '    x    ',
    'x        ',
    '         '
  ],
  [
    '            ',
    '      v     ',
    '           o',
    '@       o  x',
    '    o   x   ',
    '    x       ',
    'x           ',
    '            '
  ],
  [
    ' v           ',
    '             ',
    '             ',
    '@   h    o   ',
    '        xx   ',
    '    xx       ',
    'xx         o ',
    '           xx'
  ]
];

// Символьный реестр препятствий

const actorDict = {
  '@': Player,
  'v': VerticalFireball,
  'h': HorizontalFireball,
  'f': FireRain,
  'o': Coin
}

const parser = new LevelParser(actorDict);
runGame(schemas, parser, DOMDisplay)
  .then(() => console.log('Вы выиграли приз!'));